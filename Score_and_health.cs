using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Score_and_health : MonoBehaviour
{
    
    SpriteRenderer playerSprite;
    [SerializeField]public int score = 0;
    [SerializeField]int health;
    public Text coins;
    public Text healthText;
    public Text deadMessage;
    [SerializeField] public float counter;
    [SerializeField] public float timeToLoad =5f;
    [SerializeField] public float hitTimer;
    [SerializeField] AudioClip coinSound;
    [SerializeField] AudioClip healthSound;
       [SerializeField] AudioClip hit;
       [SerializeField] AudioClip deathPlayer;
    
    public int saved_coins;
    AudioSource audioSource;
    void Start()
    {
        counter = 0;
        playerSprite = GetComponent<SpriteRenderer>();
        health = 100;
        score +=saved_coins;
        coins.text = "Coins:" + score;
        healthText.text = "Health:" + health;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
       coins.text = "Coins:" + score;
       healthText.text = "Health:" + health;
       if(health <= 0){
         health = 0;
         onDeath();
        } 
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Coin"){
            if(audioSource.isPlaying){
                audioSource.Stop();
            }
            audioSource.PlayOneShot(coinSound);
            score +=1;
            saved_coins++;
            Destroy(other.gameObject);
        }
        if(other.gameObject.tag == "Enemy" && other.gameObject.tag != "safeEdge"){
           hitTimer += Time.deltaTime;
			 audioSource.Stop();
             audioSource.PlayOneShot(hit);
            
             if(hitTimer >= 0.05)
			   {
                if(health > 20){ 
                 health-=20;
                }else{
                    health = 0;
                }
               hitTimer = 0;
               }
        }

        if(other.gameObject.tag == "DeadLine"){
            health = 0;
        }
        if(other.gameObject.tag == "Obstacle"){
            hitTimer += Time.deltaTime;
            if(!audioSource.isPlaying){
             audioSource.PlayOneShot(hit);
            }
            if (hitTimer==0){
                health-=10;
            }
            if(hitTimer >= 0.1)
			   {
                if(health > 10){ 
                 health-=10;
                }else{
                    health = 0;
                }
                hitTimer = 0;
               }
        }

        if(other.gameObject.tag == "Health"){
            if(audioSource.isPlaying){
                audioSource.Stop();
            }
            audioSource.PlayOneShot(healthSound);
            if(health <= 50){
             health += 50;
            Destroy(other.gameObject);
            }else{
              health = 100;
              Destroy(other.gameObject);  
            }
        }
    }

    private void onDeath(){
        
        deadMessage.text = "YOU DIED! \n Press any button to continue";
        Destroy(this.gameObject);
        
    }

}
