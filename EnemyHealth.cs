using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int health;
    public bool isHit;
    // Start is called before the first frame update
    void Start()
    {
        isHit = false;
        health = 2;
    }

    void update()
    {
       
    }
    private void OnTriggerEnter2D(Collider2D other){
        if(other.gameObject.tag == "PlayerFeet"){//if player's feet hit the enemy object, the enemy's health is decreased by 1
            isHit = true;
            health-=1;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.tag == "PlayerFeet"){// resets the isHit bool when player is no longer touching the enemy
        isHit = false;
        }
    }
}

