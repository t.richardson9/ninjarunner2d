using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingGround : MonoBehaviour
{
    //Variable declaration
    Vector2 startingPosition;
    [SerializeField]  Vector2 movementVector;
    float movementFactor;
    [SerializeField] float period = 2f;
    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;//gets current positon of the object
        
    }

    // Update is called once per frame
    void Update()
    {
        if(period <= Mathf.Epsilon) {return;}
        float cycles = Time.time / period; // continuously growing over time
        const float tau = Mathf.PI * 2; // constant value of 2pi
        float rawSinWave = Mathf.Sin( cycles * tau ); // going from -1 to 1

        movementFactor = (rawSinWave + 1f)/2f;// recalculated to go from starting point to next point
                                              // otherwise starting poin is midpoint if rawsinwave varible is used only
        
        Vector2 offset = movementVector * movementFactor;//sets up the offset of how far the object will move to be controlled in the unity engine
        transform.position = startingPosition + offset; // moves the object side to side
    }

}
