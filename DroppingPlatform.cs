using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroppingPlatform : MonoBehaviour
{
    //variable declaration
    [SerializeField]  Vector2 fallingSpeed = new Vector2(0,-1);
   public Animator animator;
   [SerializeField] public float fallTimer;
   private bool isFalling;
    private void Start() {
     fallTimer = 0;
     isFalling = false;
    }

    private void Update() {
    
    destroyPlatform();//calls the destroyPlatform method every frame, but won't do anything until bool for isFalling is set to true 
    
    }
    private void OnTriggerEnter2D(Collider2D other) {// if player feet collider touches the platform, will change the bool of isFalling to true 
     if(other.gameObject.tag == "PlayerFeet"){
      transform.Translate(fallingSpeed*Time.deltaTime);
      isFalling=true;
     }
    }

    private void destroyPlatform(){  
     if(isFalling){
      fallTimer += Time.deltaTime;// makes fall timer framrate independent
      if(fallTimer >= 4){// falls for 4 seconds
       Destroy(gameObject);//after 4 seconds object is destroyed
      }
     }
    }
}
