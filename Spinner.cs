using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
[SerializeField] float spinSpeed = 1f;
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0,0,spinSpeed*Time.deltaTime); // rotates the platform by a speed to be set in unity engine
    }
}
