using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class CameraMovement : MonoBehaviour
{
    //Variable Declaration  serializedfields make the variable changable in the unity program for the instance of the object
    Rigidbody2D rb;
    BoxCollider2D bc;
    public GameObject boxCollider2d;
    public GameObject boxCollider2d2;
    private FeetCollider feetCollider;
    private bool jumpBoosting;
    private bool wallTrigger;
   [SerializeField] float moveSpeed = 1;
   [SerializeField] float jumpHeight = 6;

   [SerializeField] AudioClip running;
   [SerializeField] AudioClip jumpSound;
   [SerializeField] AudioClip jumpBoostingSound;
   [SerializeField] AudioClip jumpBoostingPickupSound;
   AudioSource audioSource;

   float jumpBoostHeight;
   public Animator animator;
   private bool facingRight = true;
  [SerializeField] private bool isGrounded;
  
   private float boostTimer;
   private int fastSpeed;
   
   [SerializeField] private bool isHit;
    
    void Start()// runs at the start of the game/creation of the object
    {
     rb = GetComponent<Rigidbody2D>();
     feetCollider = boxCollider2d.GetComponent<FeetCollider>();
     isHit = false;
     animator.SetBool("isMoving", false); 
     jumpBoosting = false;
     jumpBoostHeight = 1;
     audioSource = GetComponent<AudioSource>();
     isGrounded = feetCollider.onTheGround;
     fastSpeed = 0;
    }

    // Update is called once per frame
    void Update()
    {
        mover(); // calls mover script to move player depending on buttons player is pressing
         if (jumpBoosting){// if jump boost object is picked up by player, 10 second timer is started and jumpheight is incremented until timer is expired
			boostTimer += Time.deltaTime;
			 if(boostTimer >= 10)
			   {//resets jump to normal settings
				jumpBoostHeight = 1;
				boostTimer = 0;
				jumpBoosting = false;
               }
			}
        isGrounded = feetCollider.onTheGround;//sets the parameters for program to determine if player is touching ground or not
         if(!isGrounded){//determines if player is on the ground and sets the anitmator bools to be used to animate the player jumps or not
           animator.SetBool("isJumping", true);
       }else{
           animator.SetBool("isJumping", false);
       }
    }
    //Jump script to control the jumping mechanism if the player hits the space button
    private void Jump(){
        if(isGrounded){//makes sure the payer is grounded and doesn't allow a player to jump while in the air
         if(jumpBoosting){//if you currently have a jump boost a special sound will play
             audioSource.Stop();
              if(!audioSource.isPlaying){
                audioSource.PlayOneShot(jumpBoostingSound);   
              }
             }else{
              audioSource.Stop();//normal jump sound plays
              if(!audioSource.isPlaying){
                 audioSource.PlayOneShot(jumpSound);
              }
             }
       rb.velocity=Vector2.up * jumpHeight * jumpBoostHeight;//adds a force to the player rigidbody component to simulate a jump when the jump method is called
      
     }
    }
    
    //called to move the character left
    private void moveLeft(){
       animator.SetBool("isMoving", true);//sets animation bool to be used in animator
       transform.Translate(Vector2.left*Time.deltaTime*(moveSpeed+fastSpeed),0);//moves the character to the left and is frame rate independent
        
    }
    //called to move the character right
    private void moveRight(){
      animator.SetBool("isMoving", true);//sets animation bool to be used in animator
      transform.Translate(Vector2.right*Time.deltaTime*(moveSpeed+fastSpeed),0);//moves the character to the right and is frame rate independent
    }

    //method to flip the character sprite and components based on the direction of movement. Since there are colliders attached
    // the sprite cannot simply be inverted or the boxes will not flip and the colliders will not reflect the movement of travel
    void Flip(){
        Vector3 currentScale = gameObject.transform.localScale;//captures the current position of the object
        currentScale.x *= -1;//flips the object
        gameObject.transform.localScale = currentScale;//stores the flipped object into the currentscale of the object to reflect in the game

        facingRight = !facingRight;//swaps the bool to be used in the movement script
    }
    //This method controls the animator parameters in the collision between the player
    // and other objects in the game
    private void OnTriggerEnter2D(Collider2D other) {
     if(other.gameObject.tag == "Ground"){ // if player touches the ground then the animator will be set to not jumping
         animator.SetBool("isGrounded", true);
         
     }else{
         animator.SetBool("isGrounded", false);// else player is jumping
     }
     if(other.gameObject.tag == "Enemy"){//if player touches an enemy player hit animation is active
         isHit = true;
         animator.SetBool("isHit", true);
         
     }
     if(other.gameObject.tag == "Obstacle"){//if player touches an obstacle player hit animation is active
         isHit = true;
         animator.SetBool("isHit", true);
         
     }
     if(other.gameObject.tag == "JumpBoost"){//if player touches a jump boost object
				if(audioSource.isPlaying){//stops audio
                audioSource.Stop();
                }
                audioSource.PlayOneShot(jumpBoostingPickupSound);//plays pickup sound for jump boost
                jumpBoosting = true;//sets jumpboosting power up setting
                jumpBoostHeight = 2;
				Destroy(other.gameObject);//destroys the jumpboost object so it cannot be picked up again
			}
        if(other.gameObject.tag == "levelComplete"){//if player touches the flag, the next scene is loaded
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);//uses scene manager to load next scene
        }

    }

    //After exiting any collider the isHit animator is set top false
    private void OnTriggerExit2D(Collider2D other) {// if player stops touching enemies will trigger these changes
        isHit = false;
        animator.SetBool("isHit", false);
       
        
 
    }

    
    // Mover Method controls the player movement and sets animator parameter methods
    // also flips the sprite based on the direction the character is moving
    private void mover(){
        
            if(Input.GetKey(KeyCode.D)){// if player presses D player will move right and flip player depending on current position
                if(facingRight){
                 Flip();
                }
                moveRight();
                if(!audioSource.isPlaying){
                    if(isGrounded){ //plays footsteps while player is running and player must be grounded
                 audioSource.PlayOneShot(running);
                    }
                }
            }else if(Input.GetKey(KeyCode.A)){// if player presses A player will move Left and flip player depending on current position
                if(!audioSource.isPlaying){
                    if(isGrounded){
                 audioSource.PlayOneShot(running);//plays footsteps while player is running and player must be grounded
                    }
                }
                if (!facingRight){
                Flip();
                }
                moveLeft();
            }else{
                animator.SetBool("isMoving", false);
            }
            //if space is hit and player isGround is true player will call jump method
            if(Input.GetKey(KeyCode.Space) && isGrounded){
             Jump();
            }

            if(Input.GetKey(KeyCode.F)){// if player hits F key, player object will crouch
                animator.SetBool("isCrouching", true);
            }else{
                animator.SetBool("isCrouching", false);
               }

            if(Input.GetKey(KeyCode.LeftShift)){// if player hits the Left Shift key, player object moves at a faster run speed otherwise will move at normal speed
                fastSpeed=2;
            }else{
                fastSpeed = 0;
            }        
        }
} 
    

