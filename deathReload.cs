using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class deathReload : MonoBehaviour
{
    [SerializeField]GameObject player;
    AudioSource audioSource;
    [SerializeField] AudioClip deathSound;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)){
            Application.Quit();
        }
       if(IsNullOrDestroyed(player)){
           audioSource.PlayOneShot(deathSound);
           Invoke("reloadScene", 5);
       }
        
    }
    public static bool IsNullOrDestroyed(System.Object obj) {
 
        if (object.ReferenceEquals(obj, null)) return true;
 
        if (obj is UnityEngine.Object) return (obj as UnityEngine.Object) == null;
 
        return false;
    }

    private void reloadScene(){
        
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
         SceneManager.LoadScene(currentSceneIndex);
            
    }
    
    
}
