using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Movement2 : MonoBehaviour
{
    public Animator animator2;
      [SerializeField] float enemyMoveSpeed = 1;
      public GameObject healthScript;
      private EnemyHealth enemyHealth;
      [SerializeField] private int myEnemyHealth;
    [SerializeField] private bool facingRight;
    [SerializeField] private bool isDead;
    private float deadTimer = 0;
    [SerializeField] private float moveTimer = 0;
    [SerializeField] private float timeOfMove = 0;
    [SerializeField] private bool ishit;
    private int i;
    [SerializeField] AudioClip death;
    AudioSource audioSource;

    
    // Start is called before the first frame update
    void Start()
    {
        isDead = false;
        enemyHealth = healthScript.GetComponent<EnemyHealth>();
        facingRight = false;
        ishit = enemyHealth.isHit;
        i=1;
        audioSource =GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
       moverEnemy();
       
       myEnemyHealth = enemyHealth.health; 
       enemyDead();
       isHitAnimator();
    }

    private void enemyDead(){
        if(myEnemyHealth < 1 ){
            isDead = true;
        }
        if(isDead){
            audioSource.PlayOneShot(death);
            animator2.SetBool("isDead",true);
            deadTimer += Time.deltaTime;

			 if(deadTimer >= 1)
			   {
				Destroy(this.gameObject);
               }
			}
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
              animator2.SetBool("isFighting",true);
        }                     
    }

    private void OnTriggerExit2D(Collider2D other) {
       if(other.gameObject.tag == "Player"){
              animator2.SetBool("isFighting",false);
        }   
    }
    
    private void moverEnemy(){
        
        if(i == 1){
            Flip();
            i=2;
        }
        if(!isDead){    
         if(facingRight){
                moveRight();
               
             }else{
              moveLeft(); 
            }
             moveTimer += Time.deltaTime;
			 if(moveTimer >= timeOfMove)
			   {
				Flip();
                moveTimer = 0;
               }
        }       	       
    }
    void Flip(){
        Vector3 currentScale = gameObject.transform.localScale;
        currentScale.x *= -1;
        gameObject.transform.localScale = currentScale;

        facingRight = !facingRight;
    }
    private void moveLeft(){
       transform.Translate(Vector2.left*Time.deltaTime*enemyMoveSpeed,0);
    }

    private void moveRight(){
      transform.Translate(Vector2.right*Time.deltaTime*enemyMoveSpeed,0);
      
    }

    private void isHitAnimator(){
        if(ishit == true){
            animator2.SetBool("isHit",true);
        }else{
            animator2.SetBool("isHit",false);
        }
    }
}